import cv2 
import os
import time
import xml.etree.ElementTree as ET

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename),2)
        if img is not None:
            images.append([filename, img])
    return images


def load_xml_from_annotations(folder):
    datas = []
    for filename in os.listdir(folder):
        root       = ET.parse(folder + filename).getroot()
        img_name   = root[1].text
        img_size   = (int(root[4][0].text), int(root[4][1].text), int(root[4][2].text)) ## (width, height, depth)
        nematoides = []

        for elem in root[6:]:
            i = 6
            ## object position (xmin, ymin, xmax, ymax) infos from RectBox labelImg
            object_position = (int(root[i][4][0].text), int(root[i][4][1].text), int(root[i][4][2].text), int(root[i][4][3].text)) 
            nematoides.append(object_position)
            i += 1
        
        object_quantity = len(nematoides)
        datas.append([img_name, img_size, nematoides, object_quantity])

    return datas

def xml_image(images, xml):
    total_count = len(xml)
    data = []
    for data_images in images:
        count = 0
        for data_xml in xml:
            data_progress = []
            count += 1
            if(data_images[0] == data_xml[0]):
                data.append([data_images, data_xml[1:]])
                break
            elif total_count == count:
                data.append([data_images, []])

    return data

x_train = load_images_from_folder("./train/images/")
x_test  = load_images_from_folder("./test/images/")
y_train = load_xml_from_annotations("./train/xml/")
y_test  = load_xml_from_annotations("./test/xml/")

print(x_test)
print(y_test)