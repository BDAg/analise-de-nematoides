from PIL import Image
import os
import glob

IMAGE_PATHS = glob.glob(os.path.join('./refactored_images', '*.*'))
# print(IMAGE_PATHS)

for image_path in IMAGE_PATHS:
  if image_path.lower().endswith(('.png')):
    image = Image.open(image_path)

    number = image_path.split('/')[-1].split('.')[0]
    print(number)

    image = image.convert('RGB')

    image.save('./refactored_images/' + number + '.jpg')

    os.remove(image_path)
