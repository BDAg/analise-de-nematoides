import numpy as np
from scipy.ndimage import zoom
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import os
import cv2

def clipped_zoom(img, zoom_factor):
 
  height, width = img.shape[:2] # It's also the final desired shape
  new_height, new_width = int(height * zoom_factor), int(width * zoom_factor)

  ### Crop only the part that will remain in the result (more efficient)
  # Centered bbox of the final desired size in resized (larger/smaller) image coordinates
  y1, x1 = max(0, new_height - height) // 2, max(0, new_width - width) // 2
  y2, x2 = y1 + height, x1 + width
  bbox = np.array([y1,x1,y2,x2])
  # Map back to original image coordinates
  bbox = (bbox / zoom_factor).astype(np.int)
  y1, x1, y2, x2 = bbox
  cropped_img = img[y1:y2, x1:x2]

  # Handle padding when downscaling
  resize_height, resize_width = min(new_height, height), min(new_width, width)
  pad_height1, pad_width1 = (height - resize_height) // 2, (width - resize_width) //2
  pad_height2, pad_width2 = (height - resize_height) - pad_height1, (width - resize_width) - pad_width1
  pad_spec = [(pad_height1, pad_height2), (pad_width1, pad_width2)] + [(0,0)] * (img.ndim - 2)

  result = cv2.resize(cropped_img, (resize_width, resize_height))
  result = np.pad(result, pad_spec, mode='constant')
  assert result.shape[0] == height and result.shape[1] == width
  return result

def normalize(x):
  return np.array((x - np.min(x)) / (np.max(x) - np.min(x)))

for image_dir in os.listdir('./images'):
  image = mpimg.imread('./images/' + image_dir)

  image_zoom = clipped_zoom(image, 1.25)

  image_zoom = normalize(image_zoom)

  image_dir_splited = image_dir.split('.')

  plt.imsave('./refactored_images/' + image_dir_splited[0] + '.png', image_zoom)
