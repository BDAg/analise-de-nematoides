from flask import Flask, render_template, url_for, request, redirect

import os
import inference

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        uploaded_file = request.files['file']

        if uploaded_file.filename != '':
            image_path = os.path.join('static/images', uploaded_file.filename)
            uploaded_file.save(image_path)
            image = inference.run_inference(image_path)

            result = {
                'image_path': image
            }

            return render_template('result.html', result=result)
            
    return render_template('home.html')

if __name__ == '__main__':
    app.run(debug=True)